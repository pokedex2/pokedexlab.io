const formulario = document.querySelector('#formulario');
const boton = document.querySelector('#boton');

class Pokemones {
    template = "";
    static drawPokemonsTemplate(data) {
        this.template = ""
            
            data.results.map((dato) => {
                let query = formulario.value.toLowerCase();
                let name =  dato.name;
                
                if (query != ''){
                    if (name.indexOf(query) !== -1){
                      this.fetchPokemonData(dato.url)
                    }
                } else {
                    this.fetchPokemonData(dato.url)
                }
                
            })
 
    }

    static traemeLosPokemones() {
        document.querySelector("#listado").innerHTML = ""
        fetch("https://pokeapi.co/api/v2/pokemon?limit=50")
            .then(response => response.json())
            .then(datos => {
                Pokemones.drawPokemonsTemplate(datos);
            })
    }
    static fetchPokemonData(url) {
       // console.log("Esta es la API" + url);
        fetch(url)
            .then(response => response.json())
            .then(pokemon => {
                this.template += this.renderPokemon(pokemon); 
                document.querySelector("#listado").innerHTML = this.template;   
                                    
            }
            )
    }

    static renderPokemon(pokemon) {
        let types = "";
        pokemon.types.forEach(element =>  types += `<span class="${element.type.name}">` + element.type.name + "</span>")
        let template = 
         `
                <div class="box col-sm-12 col-lg-3" id="resultado">
                    <div class="img">
                        <img src="https://img.pokemondb.net/artwork/${pokemon.name}.jpg" />  
                    </div>
                    <div class="info">
                        <div class="number">N.°${pokemon.id}</div>
                        <div class="name">${pokemon.name} </div>
                        <div class="types">${types}</div> 
                    </div>           
                </div>
            `                   
        return template;
        
    }

    static main() {
        this.traemeLosPokemones();
    }
}

function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }

Pokemones.main();
boton.addEventListener('click', Pokemones.traemeLosPokemones);
boton.addEventListener('touch', Pokemones.traemeLosPokemones);
